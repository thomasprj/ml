package com.mgd.ml

import com.mgd.core.services.RuntimeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class)

    static void main(String[] args) {
		SpringApplication.run(Application.class, args)
		RuntimeService.logMemoryStats()
	}

	@Override
	void run(String... args) throws Exception {

		if (!isUnitTesting()) {
            log.info("Machine learning PoC successfully started up!")
        }
	}

    /**
     * Determine whether unit tests are currently executing.
     */
    private static boolean isUnitTesting() {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace()
        List<StackTraceElement> list = Arrays.asList(stackTrace)
        for (StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true
            }
        }
        return false
    }
}
