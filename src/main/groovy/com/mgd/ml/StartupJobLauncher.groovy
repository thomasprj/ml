package com.mgd.ml

import com.mgd.ml.service.PriceHistoryService
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

/**
 * The StartupJobLauncher listens to the ApplicationContextLoaded
 * event and launches any Spring Batch recipe jobs or tasks specified in configuration.
 *
 * It also manages the lifecycle of KCL worker threads for DynamoDB Stream processing.
 */
@Component
class StartupJobLauncher {
	
	@Autowired private PriceHistoryService priceHistoryService

	@Value('${launch_at_startup.price_history:false}') private Boolean launchGeneratePriceHistory

    private static final TimeZone timeZoneMGD = TimeZone.getTimeZone("US/Central")
    private static final DateTimeZone dateTimeZoneMGD = DateTimeZone.forTimeZone(timeZoneMGD)

    private static final Logger log = LoggerFactory.getLogger(StartupJobLauncher.class)

	/**
	 * Invoke jobs at application startup (e.g. on Spring context refresh event).
     *
     * @param event   the event payload
	 */
	@EventListener
    void handleContextRefresh(ContextRefreshedEvent event ) {

        log.info("Launching startup jobs...")

		// don't execute any startup tasks if unit tests are running
		if (isUnitTesting()) {
			log.warn("Application is executing in unit test context; startup jobs will be skipped.")
			return
		}

		// launch generate Deal Price History task
        if (launchGeneratePriceHistory) {

            def currentDate = new DateTime()

            DateTime startDateTime = currentDate.minusWeeks(1)
                    .withZone(dateTimeZoneMGD)
                    .withTimeAtStartOfDay()

            DateTime endDateTime = startDateTime.plusWeeks(1)

            def results = priceHistoryService.generatePriceHistoryArchive(startDateTime.millis, endDateTime.millis)
            log.info("A price history archive file named ${results.archiveFilename} was successfully created and posted to S3: ${results.archiveCount} deals were archived; ${results.skipCount} deals were skipped")
        }
	}

	/**
	 * Determine whether unit tests are currently executing.
	 */
	private static boolean isUnitTesting() {

		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace()
		List<StackTraceElement> list = Arrays.asList(stackTrace)
		for (StackTraceElement element : list) {
			if (element.getClassName().startsWith("org.junit.")) {
				return true
			}
		}
		return false
	}
}
