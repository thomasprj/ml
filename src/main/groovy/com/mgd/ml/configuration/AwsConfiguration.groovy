package com.mgd.ml.configuration

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * AwsConfiguration holds Spring Bean definitions for Amazon Web Services Java SDK classes.
 */
@Configuration
class AwsConfiguration {

    @Bean
    AmazonS3 s3Client() {
        return new AmazonS3Client(new DefaultAWSCredentialsProviderChain())
    }

    @Bean
    DynamoDBMapper dynamoDBMapper() {
        return new DynamoDBMapper(new AmazonDynamoDBClient(
                new DefaultAWSCredentialsProviderChain()))
    }

    @Bean
    DynamoDB dynamoDB() {
        return new DynamoDB(new AmazonDynamoDBClient(
                new DefaultAWSCredentialsProviderChain()))
    }

    @Bean
    AmazonDynamoDBClient dynamoDBClient() {
        return new AmazonDynamoDBClient(
                new DefaultAWSCredentialsProviderChain())
    }
}
