package com.mgd.ml.configuration.autoconfigure.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Spring Boot Conditional Annotation used to enable and disable MGD scheduled tasks
 *
 *  Individual schedules can be turned on or off with a specific property for each task
 *  A global property can be used to define global settings for all tasks (e.g. tasks.enable.all: true)
 *  The property value for the individual task will always override the global setting (e.g. tasks.enable.task.<task name>: false)
 *  The matchIfMissing flag is an optional argument to make the task opt-out or opt-in
 *  The default implementation is opt-out (e.g. the task will be created if both properties are missing)
 *
 *  NOTE: Implemented in Java to mitigate Groovy compilation errors
 */
@Conditional(OnTaskPropertyCondition.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ConditionalOnTaskProperty {
    String name() default "";
    boolean matchIfMissing() default true;
}
