package com.mgd.ml.configuration.autoconfigure.condition

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionOutcome
import org.springframework.boot.autoconfigure.condition.SpringBootCondition
import org.springframework.context.annotation.ConditionContext
import org.springframework.core.annotation.AnnotationAttributes
import org.springframework.core.type.AnnotatedTypeMetadata

/**
 * Created by petert on 2015-12-13.
 */
class OnTaskPropertyCondition extends SpringBootCondition  {

    private static final String ALL_TASKS_PROPERTY = "tasks.enable.all"

    private static final Logger log = LoggerFactory.getLogger(OnTaskPropertyCondition.class)

    @Override
    ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {

        // Custom condition to be used to determine whether to enable a scheduled task
        // Individual schedules can be turned on or off with a specific property for each task
        // A global property can be used to define global settings for all tasks (e.g. tasks.enable.all: true)
        // The property value for the individual task will always override the global setting (e.g. tasks.enable.task.<task name>: false)
        // The matchIfMissing flag is an optional argument to make the task opt-out or opt-in
        // The default implementation is opt-out (e.g. the task will be created if both properties are missing)

        // get the annotation attributes
        def attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(ConditionalOnTaskProperty.class.getName()))
        def matchIfMissing = attributes.getBoolean("matchIfMissing")
        def systemProperty = attributes.getString("name")

        // get the property values from Spring Boot context
        def resolver = context.getEnvironment()
        def propertyValue = resolver.getProperty(systemProperty)
        def allTasksPropertyValue = resolver.getProperty(ALL_TASKS_PROPERTY)

        log.debug("Evaluating conditional bean creation: ${systemProperty} = ${propertyValue}; ${ALL_TASKS_PROPERTY} = ${allTasksPropertyValue}")

        // determine whether task should be enabled
        if (propertyValue != null)
            // if the property value exists for the task, it overrides everything else
            return checkProperty(systemProperty, propertyValue)
        else
            if (allTasksPropertyValue != null)
                // property value is missing, but All Tasks property exists
                return checkProperty(ALL_TASKS_PROPERTY, allTasksPropertyValue)
            else
                // both property values are missing in the configuration
                if (matchIfMissing) {
                    log.debug("No matches found for conditional property ${systemProperty}; creating Spring Bean due to opt-out flag...")
                    return ConditionOutcome.match()
                }
                else {
                    log.debug("No matches found for conditional property ${systemProperty}; cancelling Spring Bean creation due to opt-in flag")
                    return ConditionOutcome.noMatch("No property values found for '${systemProperty}'")
                }
    }

    // local helper method to determine whether property is true or false
    // it will return true if the property value is set to anything but "false"
    private ConditionOutcome checkProperty(String systemProperty, String propertyValue) {

        if (propertyValue.toLowerCase() == "false") {
            log.debug("Cancelling Spring Bean creation due to conditional property ${systemProperty}")
            return ConditionOutcome.noMatch("${systemProperty} was set to false")
        }
        else {
            log.debug("Conditional property ${systemProperty} evaluated to true; creating Spring Bean...")
            return ConditionOutcome.match()
        }
    }
}
