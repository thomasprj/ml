package com.mgd.ml.model

/**
 * Represents the current status of a Deal in the publishing queue
 */
enum DealQueueState {

    WAITING (0),
    PUBLISHED (1),
    UNPUBLISHED (2),
    REPUBLISHED (3)

    final Integer id
    static final Map map
    def dealQueueStates = ["Waiting", "Published", "Unpublished", "Republished"]

    static {
        map = [:] as TreeMap
        values().each{ dealQueueState ->
            map.put(dealQueueState.id, dealQueueState)
        }
    }

    private DealQueueState(Integer id) {
        this.id = id
    }

    static DealQueueState fromId(final Integer id) {
        return map[id]
    }

    static DealQueueState getDealQueueState(final Integer id) {
        return fromId(id)
    }

    @Override
    String toString() {
        return dealQueueStates[id]
    }
}
