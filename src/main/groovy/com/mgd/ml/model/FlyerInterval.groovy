package com.mgd.ml.model

/**
 * The FlyerInterval enumeration describes the frequency of a
 * published Flyer. The majority of FLyers will have a Weekly
 * publishing cycle. Custom Flyers are ad-hoc Flyers created
 * on the fly with no recurring schedule.
 */
enum FlyerInterval {

    WEEKLY (0),
    BIWEEKLY (1),
    MONTHLY (2),
    QUARTERLY (3),
    SEMIANNUALLY (4),
    ANNUALLY (5),
    CUSTOM (6)

    final Integer id
    static final Map map
    def intervalNames = ["Weekly", "Bi-Weekly", "Monthly", "Quarterly", "Semi-Annual", "Annual", "Custom"]

    static {
        map = [:] as TreeMap
        values().each{ flyerInterval ->
            map.put(flyerInterval.id, flyerInterval)
        }
    }

    private FlyerInterval(Integer id) {
        this.id = id
    }

    static FlyerInterval fromId(final Integer id) {
        return map[id]
    }

    static getFlyerInterval(final Integer id) {
        return fromId(id)
    }

    @Override
    String toString() {
        return intervalNames[id]
    }
}
