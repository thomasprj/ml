package com.mgd.ml.model

import org.joda.time.DateTime

/**
 * The FlyerWeekDay enumeration describes the day of the week
 * on which the Flyer is published. It follows the JavaScript
 * convention, whereby Sunday has a value of 0.
 */
enum StartWeekDay {

    SUNDAY (0),
    MONDAY (1),
    TUESDAY (2),
    WEDNESDAY (3),
    THURSDAY (4),
    FRIDAY (5),
    SATURDAY (6)

    final Integer id
    static final Map map
    static final startWeekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

    static {
        map = [:] as TreeMap
        values().each{ startWeekDay ->
            map.put(startWeekDay.id, startWeekDay)
        }
    }

    private StartWeekDay(Integer id) {
        this.id = id
    }

    static StartWeekDay fromId(final Integer id) {
        return  map[id]
    }

    static StartWeekDay fromISODayOfWeek(final Integer dayOfWeek) {

        // ISO DayOfWeek is from Monday to Sunday
        // where Monday = 1
        def id = dayOfWeek
        if (id == 7) {
            id = 0
        }

        return fromId(id)
    }

    static StartWeekDay fromDateTime(final DateTime flyerDate) {

        return fromISODayOfWeek(flyerDate.dayOfWeek)
    }

    static StartWeekDay getStartWeekDay(final Integer id) {
        return fromId(id)
    }

    Integer toISODayOfWeek() {

        // ISO DayOfWeek is from Monday to Sunday
        // where Monday = 1
        if (id == 0) {
            return 7
        } else {
            return id
        }
    }

    Integer toJavaScriptDayOfWeek() {
        return id
    }

    @Override
    String toString() {
        return startWeekDays[id]
    }
}
