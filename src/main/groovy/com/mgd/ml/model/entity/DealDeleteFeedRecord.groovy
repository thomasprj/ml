package com.mgd.ml.model.entity

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * The DealDeleteFeedRecord describes an XML document which contains
 * the StoreID (a.k.a. market area id) to remove from a retailer offer.
 * These records are serialized to the Delete feed file.
 */
@JacksonXmlRootElement(localName="StoreSpecial")
class DealDeleteFeedRecord {

    @JacksonXmlProperty(localName="SpecialID", isAttribute=true)
    String id

    @JacksonXmlProperty(localName="StoreID", isAttribute=true)
    Integer marketAreaId


    // constructor
    DealDeleteFeedRecord(String id, Integer marketAreaId) {
        this.id = id
        this.marketAreaId = marketAreaId
    }
}
