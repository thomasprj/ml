package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore

/**
 * Flyers represent a published list of offers for a specified
 * retailer.Flyers can be published on a number of different cycles as per
 * their FlyerSchedule, but are typically published weekly.
 * <p>
 * The Flyer class is annotated with DynamoDB mapper
 * annotations. The DynamoDBTable annotation is a placeholder
 * for the real DynamoDB table name, which will be injected
 * at runtime based on configuration values.
 */
abstract class AbstractFlyer {

    // record key
    String id

    // core data
    Integer chainId
    String chainName
    String chainImageUrl
    String name
    Integer marketAreaId
    Integer rootMarketAreaId
    Long startDate
    Long endDate
    String description
    String marketAreaName
    String stateAbbrev
    String alias
    Integer priority
    Boolean inheritsDatesFromParent
    Boolean publishesWithParent
    String flyerUrl
	List<Note> notes
    List<String> parentFlyers
    List<FlyerNode> children

    // default constructor
    AbstractFlyer() {}

    @DynamoDBIgnore
    String getCompoundKey() {
        return (chainId && marketAreaId) ? "${chainId.toString().padLeft(4, "0")}-${marketAreaId.toString().padLeft(6, "0")}" : null
    }
    void setCompoundKey(String compoundId) {

        def ids = compoundId.tokenize('-')
        if (ids && ids.size() == 2) {
            chainId = ids[0].toInteger()
            marketAreaId = ids[1].toInteger()
        } else {
            throw new Exception("Unable to create FlyerVersion keys from compoundKey value: '${compoundId}'")
        }
    }

    @DynamoDBIgnore
    String getUuid() {
        return (chainId && marketAreaId) ? "00000000-0000-0000-${chainId.toString().padLeft(4, "0")}-${marketAreaId.toString().padLeft(12, "0")}" : null
    }
    void setUuid(String uuid) {

        def ids = uuid.tokenize('-')
        if (ids && ids.size() == 5) {
            chainId = ids[3].toInteger()
            marketAreaId = ids[4].toInteger()
        } else {
            throw new Exception("Unable to create FlyerVersion keys from uuid value: '${uuid}'")
        }
    }

    // IMPORTANT: concrete implementations should override the field accessors,
    //            annotated with the appropriate DynamoDB annotation
    //            e.g. @DynamoDBHashKey(attributeName="id") OR @DynamoDBAttribute(attributeName="id")

    String getId() { return id }
    void setId(String id) { this.id = id }

    Integer getChainId() { return chainId }
    void setChainId(Integer chainId) { this.chainId = chainId }

    String getChainName() { return chainName }
    void setChainName(String chainName) { this.chainName = chainName }

    String getChainImageUrl() { return chainImageUrl }
    void setChainImageUrl(String chainImageUrl) { this.chainImageUrl = chainImageUrl }

    String getName() { return name }
    void setName(String name) { this.name = name }

    Integer getMarketAreaId() { return marketAreaId }
    void setMarketAreaId(Integer marketAreaId) { this.marketAreaId = marketAreaId }

    Integer getRootMarketAreaId() { return rootMarketAreaId }
    void setRootMarketAreaId(Integer rootMarketAreaId) { this.rootMarketAreaId = rootMarketAreaId }

    Long getStartDate() { return startDate }
    void setStartDate(Long startDate) { this.startDate = startDate }

    Long getEndDate() { return endDate }
    void setEndDate(Long endDate) { this.endDate = endDate }

    String getDescription() { return description }
    void setDescription(String description) { this.description = description }

    String getMarketAreaName() { return marketAreaName }
    void setMarketAreaName(String marketAreaName) { this.marketAreaName = marketAreaName }

    String getStateAbbrev() { return stateAbbrev }
    void setStateAbbrev(String stateAbbrev) { this.stateAbbrev = stateAbbrev }

    String getAlias() { return alias }
    void setAlias(String alias) { this.alias = alias }

    Integer getPriority() { return priority }
    void setPriority(Integer priority) { this.priority = priority }

    Boolean getInheritsDatesFromParent() { return inheritsDatesFromParent }
    void setInheritsDatesFromParent(Boolean inheritsDatesFromParent) { this.inheritsDatesFromParent = inheritsDatesFromParent }

    Boolean getPublishesWithParent() { return publishesWithParent }
    void setPublishesWithParent(Boolean publishesWithParent) { this.publishesWithParent = publishesWithParent }

    String getFlyerUrl() { return flyerUrl }
    void setFlyerUrl(String flyerUrl) { this.flyerUrl = flyerUrl }

	List<Note> getNotes() { return notes }
	void setNotes(List<Note> notes) { this.notes = notes }

    List<String> getParentFlyers() { return parentFlyers }
    void setParentFlyers(List<String> parentFlyers) { this.parentFlyers = parentFlyers }

    List<FlyerNode> getChildren() { return children }
    void setChildren(List<FlyerNode> children) { this.children = children }
}