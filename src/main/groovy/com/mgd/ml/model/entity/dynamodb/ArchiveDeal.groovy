package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.*
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType
import com.google.common.primitives.Doubles
import groovy.json.JsonSlurper
import org.apache.commons.io.Charsets
import org.apache.commons.io.IOUtils

import java.util.zip.GZIPInputStream

/**
 * The ArchiveDeal describes a DynamoDB document which contains the
 * details of a retailer offer, suitable for historical storage.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class ArchiveDeal {

    // guid
    @DynamoDBHashKey(attributeName="id")
    String id

    // product info
    String brand
    String majorCategoryId
    String majorCategoryName
    Integer majorCategorySort
    String description
    String originalDescription
    String imageUrl
    String manufacturer
    String name
    Float size
    String uom
    String count
    String container
    String upc

    // deal info
    Integer bonusType
    String bonusDeal
    Integer minimum
    Long saleEnd
    Long saleStart

    // store info
    String chainName
    List<Integer> marketAreaIds
    List<DealMarketArea> marketAreas

    // data entry info
    Long dateCreated
    Long dateUpdated
    String flyerId
    Float timeToEnter
    String userId
    String teamId
    Integer queueState
    Integer state
    Integer saleType

    // additional text
    String additional
    List<String> additionalArray
    Integer limit

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean requiresCoupon

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean requiresLoyaltyCard

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean requiresMinimum

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean topDeal

    // sale price info
    String customPrice
    String salePrice
    Float saleQuantity
    String saleUom

    // internal values for serializing to Meredith feed
    @DynamoDBIgnore
    Integer xmlSaleQuantity

    @DynamoDBIgnore
    Float xmlSalePrice

    // internal values for serializing to Elasticsearch
    @DynamoDBIgnore
    Float esSaleQuantity

    @DynamoDBIgnore
    Float esSalePrice // SalePrice should eventually be changed to a String type for Elasticsearch

    // internal value for GZIP compressed version of marketAreas array
    byte[] compressedMarketAreas

    @DynamoDBIgnore
    Map toMap() {
        def object = this
        return object?.properties.findAll{ (it.key != 'class') }.collectEntries {
            it.value == null || it.value instanceof Serializable ? [it.key, it.value] : [it.key, toMap(it.value)]
        }
    }

    @DynamoDBIgnore
    @Override
    String toString() {
        return toMap().toString()
    }

    @DynamoDBAttribute(attributeName = "compressedMarketAreas")
    byte[] getCompressedMarketAreas() { return compressedMarketAreas }
    void setCompressedMarketAreas(byte[] bytes) {

        // use GZIP to uncompress to a UTF-8 encoded string
        ByteArrayOutputStream os = new ByteArrayOutputStream(bytes.length)
        IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(bytes)), os)
        String json = new String(os.toByteArray(), Charsets.UTF_8)

        // create a map from the JSON string
        def marketAreasJson = new JsonSlurper().parseText(json)

        //initialize the marketAreas array from the map
        List<DealMarketArea> marketAreas = new ArrayList<DealMarketArea>()
        marketAreasJson.each { marketAreaNode ->
            def marketArea = new DealMarketArea((Map)marketAreaNode)
            marketAreas << marketArea
        }

        this.marketAreas = marketAreas
    }

    @DynamoDBAttribute(attributeName = "salePrice")
    String getSalePrice() { return salePrice }
    void setSalePrice(String salePrice) {

        this.salePrice = salePrice

        // default to "0.00" if salePrice is non-numeric
        if ( Doubles.tryParse(salePrice) ) {
            this.esSalePrice = salePrice.toFloat()
            this.xmlSalePrice = salePrice.toFloat()
        }
        else {
            this.esSalePrice = null
            this.xmlSalePrice = 0.00f
        }
    }

    @DynamoDBAttribute(attributeName = "saleQuantity")
    Float getSaleQuantity() { return saleQuantity }
    void setSaleQuantity(Float saleQuantity) {

        this.saleQuantity = saleQuantity
        this.esSaleQuantity = saleQuantity

        // default to 1 if saleQuantity is not a whole number
        if ( (saleQuantity % 1) == 0 ) {
            this.xmlSaleQuantity = saleQuantity
        } else {
            this.xmlSaleQuantity = 1
        }
    }
}
