package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument

@DynamoDBDocument
class Author {

	String id
	String email
	String firstName
	String lastName
	String teamId
	String teamName
	List<String> permissions
	List<String> roles
	Long dateUpdated
}