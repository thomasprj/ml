package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * Created by petert on 2016-03-11.
 */
// real table name will be injected when the record is saved, based on config values
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class Chain {

    @DynamoDBHashKey(attributeName="id")
    Integer id

    String name
    String imageUrl
}