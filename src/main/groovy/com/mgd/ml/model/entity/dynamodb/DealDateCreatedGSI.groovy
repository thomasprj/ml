package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * The DealDateCreatedIdx describes a DynamoDB document which contains the
 * contents of the dateCreated-index GSI.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class DealDateCreatedGSI {

    String id
    Integer queueState

    @DynamoDBHashKey(attributeName="dateCreated")
    Long dateCreated
}
