package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore

/**
 * DealMarketArea represents a MarketArea attached to a deal. The
 * Deal entity will be decorated with a list of MarketAreas to
 * represent the zone of geographic coverage for the retail offer.
 *
 * The geographic coverage is described by a list of ZIP codes
 * served by the MarketArea.
 */
@DynamoDBDocument
class DealMarketArea {

    Integer id
    String name
    String alias
    String stateAbbrev
    List<String> zips

    @DynamoDBIgnore
    Map toMap() {
        def object = this
        return object?.properties.findAll{ (it.key != 'class') }.collectEntries {
            it.value == null || it.value instanceof Serializable ? [it.key, it.value] : [it.key, toMap(it.value)]
        }
    }

    @DynamoDBIgnore
    @Override
    String toString() {
        return toMap().toString()
    }
}
