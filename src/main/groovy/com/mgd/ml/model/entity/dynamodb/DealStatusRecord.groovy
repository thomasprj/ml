package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * The Deal describes a DynamoDB document which contains the
 * details of a retailer offer.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class DealStatusRecord {

    // guid
    @DynamoDBHashKey(attributeName="id")
    String id

    // data entry info
    Long dateCreated
    Long dateUpdated
    Integer queueState


    @DynamoDBIgnore
    Map toMap() {
        def object = this
        return object?.properties.findAll{ (it.key != 'class') }.collectEntries {
            it.value == null || it.value instanceof Serializable ? [it.key, it.value] : [it.key, toMap(it.value)]
        }
    }

    @DynamoDBIgnore
    @Override
    String toString() {
        return toMap().toString()
    }
}
