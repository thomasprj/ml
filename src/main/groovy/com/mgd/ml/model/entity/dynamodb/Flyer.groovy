package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.*
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType

/**
 * Flyers represent a published list of offers for a specified
 * retailer.Flyers can be published on a number of different cycles as per
 * their FlyerSchedule, but are typically published weekly.
 * <p>
 * The Flyer class is annotated with DynamoDB mapper
 * annotations. The DynamoDBTable annotation is a placeholder
 * for the real DynamoDB table name, which will be injected
 * at runtime based on configuration values.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class Flyer extends AbstractFlyer{

    // extended attributes
    Long dateUpdated = null
    Boolean published = null

    // helper object
    LegacyFlyerSchedule flyerSchedule

    Flyer(String flyerId, LegacyFlyerSchedule flyerSchedule, FlyerVersion flyerVersion, List<String> parentFlyers) {

        this.id = flyerId
        this.chainId = flyerVersion.chainId
        this.chainName = flyerVersion.chainName
        this.chainImageUrl = flyerVersion.chainImageUrl
        this.name = flyerVersion.description
        this.marketAreaId = flyerVersion.marketAreaId
        this.rootMarketAreaId = flyerVersion.rootMarketAreaId

        this.startDate = flyerSchedule.flyerStartDate.getMillis()
        this.endDate = flyerSchedule.flyerEndDate.getMillis()

        this.description = "${flyerSchedule.flyerInterval.toString()} Flyer for ${flyerVersion.description}"
        this.marketAreaName = flyerVersion.marketAreaName
        this.stateAbbrev = flyerVersion.stateAbbrev
        this.alias = flyerVersion.alias
        this.priority = flyerVersion.priority
        this.inheritsDatesFromParent = flyerVersion.inheritsDatesFromParent
        this.publishesWithParent = flyerVersion.publishesWithParent
        this.flyerUrl = flyerVersion.flyerUrl
		this.notes = flyerVersion.notes

        this.parentFlyers = parentFlyers
        this.flyerSchedule = flyerSchedule
    }

    // default constructor
    Flyer() { super() }

    @DynamoDBAttribute(attributeName="dateUpdated")
    Long getDateUpdated() { return this.dateUpdated }
    void setDateUpdated(Long dateUpdated) { this.dateUpdated = dateUpdated }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName="published")
    Boolean getPublished() { return this.published }
    void setPublished(Boolean published) { this.published = published }

    @DynamoDBIgnore
    LegacyFlyerSchedule getFlyerSchedule() { return flyerSchedule }
    void setFlyerSchedule(LegacyFlyerSchedule flyerSchedule) { this.flyerSchedule = flyerSchedule }


    // overridden accessors for CoreDynamoDBFlyer abstract class
    @DynamoDBHashKey(attributeName="id")
    String getId() { return super.getId() }
    void setId(String id) { super.setId(id) }

    @DynamoDBAttribute(attributeName="chainId")
    Integer getChainId() { return super.getChainId() }
    void setChainId(Integer chainId) { super.setChainId(chainId) }

    @DynamoDBAttribute(attributeName="chainName")
    String getChainName() { return super.getChainName() }
    void setChainName(String chainName) { super.setChainName(chainName) }

    @DynamoDBAttribute(attributeName="chainImageUrl")
    String getChainImageUrl() { return super.getChainImageUrl() }
    void setChainImageUrl(String chainImageUrl) { super.setChainImageUrl(chainImageUrl) }

    @DynamoDBAttribute(attributeName="name")
    String getName() { return super.getName() }
    void setName(String name) { super.setName(name) }

    @DynamoDBAttribute(attributeName="marketAreaId")
    Integer getMarketAreaId() { return super.getMarketAreaId() }
    void setMarketAreaId(Integer marketAreaId) { super.setMarketAreaId(marketAreaId) }

    @DynamoDBAttribute(attributeName="rootMarketAreaId")
    Integer getRootMarketAreaId() { return super.getRootMarketAreaId() }
    void setRootMarketAreaId(Integer rootMarketAreaId) { super.setRootMarketAreaId(rootMarketAreaId) }

    @DynamoDBAttribute(attributeName="startDate")
    Long getStartDate() { return super.getStartDate() }
    void setStartDate(Long startDate) { super.setStartDate(startDate) }

    @DynamoDBAttribute(attributeName="endDate")
    Long getEndDate() { return super.getEndDate() }
    void setEndDate(Long endDate) { super.setEndDate(endDate) }

    @DynamoDBAttribute(attributeName="description")
    String getDescription() { return super.getDescription() }
    void setDescription(String description) { super.setDescription(description) }

    @DynamoDBAttribute(attributeName="marketAreaName")
    String getMarketAreaName() { return super.marketAreaName }
    void setMarketAreaName(String marketAreaName) { super.setMarketAreaName(marketAreaName) }

    @DynamoDBAttribute(attributeName="stateAbbrev")
    String getStateAbbrev() { return super.stateAbbrev }
    void setStateAbbrev(String stateAbbrev) { super.setStateAbbrev(stateAbbrev) }

    @DynamoDBAttribute(attributeName="alias")
    String getAlias() { return super.alias }
    void setAlias(String alias) { super.setAlias(alias)  }

    @DynamoDBAttribute(attributeName="priority")
    Integer getPriority() { return super.getPriority() }
    void setPriority(Integer priority) { super.setPriority(priority) }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName="inheritsDatesFromParent")
    Boolean getInheritsDatesFromParent() { return super.getInheritsDatesFromParent() }
    void setInheritsDatesFromParent(Boolean inheritsDatesFromParent) { super.setInheritsDatesFromParent(inheritsDatesFromParent) }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName="publishesWithParent")
    Boolean getPublishesWithParent() { return super.getPublishesWithParent() }
    void setPublishesWithParent(Boolean publishesWithParent) { super.setPublishesWithParent(publishesWithParent) }

    @DynamoDBAttribute(attributeName="flyerUrl")
    String getFlyerUrl() { return super.getFlyerUrl() }
    void setFlyerUrl(String flyerUrl) { super.setFlyerUrl(flyerUrl) }

	@DynamoDBAttribute(attributeName="notes")
	List<Note> getNotes() { return super.getNotes() }
	void setNotes(List<Note> notes) { super.setNotes(notes) }

    @DynamoDBAttribute(attributeName="parentFlyers")
    List<String> getParentFlyers() { return super.getParentFlyers() }
    void setParentFlyers(List<String> parentFlyers) { super.setParentFlyers(parentFlyers) }

    @DynamoDBAttribute(attributeName = "children")
    List<FlyerNode> getChildren() { return super.getChildren() }
    void setChildren(List<FlyerNode> children) { super.setChildren(children) }
}
