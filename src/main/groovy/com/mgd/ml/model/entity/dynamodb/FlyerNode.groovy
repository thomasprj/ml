package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped

/**
 * The FlyerNode describes a DynamoDB document which contains the
 * parent-child relationship hierarchy for Flyers.
 * <p>
 * The FlyerNode class is annotated with DynamoDB mapper
 * annotations and is referenced as a field in other DynamoDB
 * tables.
 */
@DynamoDBDocument
class FlyerNode extends AbstractFlyer{

    // extended data
    String parentId
    Boolean published
    Boolean current


    FlyerNode() {
        this.chainId = null
        this.marketAreaId = null
        this.parentId = null
        this.id = null
        children = new ArrayList<FlyerNode>()
    }

    FlyerNode(String compoundId, String parentId) {

        setCompoundKey(compoundId)
        this.parentId = parentId
        children = new ArrayList<FlyerNode>()
    }

    FlyerNode(Flyer flyer) {

        this.id = flyer.id
        this.chainId = flyer.chainId
        this.chainName = flyer.chainName
        this.chainImageUrl = flyer.chainImageUrl
        this.name = flyer.name
        this.marketAreaId = flyer.marketAreaId
        this.rootMarketAreaId = flyer.rootMarketAreaId

        this.startDate = flyer.startDate
        this.endDate = flyer.endDate

        this.description = flyer.description
        this.marketAreaName = flyer.marketAreaName
        this.stateAbbrev = flyer.stateAbbrev
        this.alias = flyer.alias
        this.priority = flyer.priority
        this.inheritsDatesFromParent = flyer.inheritsDatesFromParent
        this.publishesWithParent = flyer.publishesWithParent
        this.flyerUrl = flyer.flyerUrl
		this.notes = flyer.notes

        this.children = new ArrayList<FlyerNode>()

        this.published = false
    }

    @DynamoDBAttribute(attributeName = "parentId")
    String getParentId() { return parentId }
    void setParentId(String parentId) { this.parentId = parentId }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName = "published")
    Boolean getPublished() { return published }
    void setPublished(Boolean published) { this.published = published }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName = "current")
    Boolean getCurrent() { return current }
    void setCurrent(Boolean current) { this.current = current }


    // overridden accessors for CoreDynamoDBFlyer abstract class
    @DynamoDBAttribute(attributeName = "id")
    String getId() { return super.getId() }
    void setId(String id) { super.setId(id) }

    @DynamoDBAttribute(attributeName="chainId")
    Integer getChainId() { return super.getChainId() }
    void setChainId(Integer chainId) { super.setChainId(chainId) }

    @DynamoDBAttribute(attributeName="chainName")
    String getChainName() { return super.getChainName() }
    void setChainName(String chainName) { super.setChainName(chainName) }

    @DynamoDBAttribute(attributeName="chainImageUrl")
    String getChainImageUrl() { return super.getChainImageUrl() }
    void setChainImageUrl(String chainImageUrl) { super.setChainImageUrl(chainImageUrl) }

    @DynamoDBAttribute(attributeName="name")
    String getName() { return super.getName() }
    void setName(String name) { super.setName(name) }

    @DynamoDBAttribute(attributeName="marketAreaId")
    Integer getMarketAreaId() { return super.getMarketAreaId() }
    void setMarketAreaId(Integer marketAreaId) { super.setMarketAreaId(marketAreaId) }

    @DynamoDBAttribute(attributeName="rootMarketAreaId")
    Integer getRootMarketAreaId() { return super.getRootMarketAreaId() }
    void setRootMarketAreaId(Integer rootMarketAreaId) { super.setRootMarketAreaId(rootMarketAreaId) }

    @DynamoDBAttribute(attributeName="startDate")
    Long getStartDate() { return super.getStartDate() }
    void setStartDate(Long startDate) { super.setStartDate(startDate) }

    @DynamoDBAttribute(attributeName="endDate")
    Long getEndDate() { return super.getEndDate() }
    void setEndDate(Long endDate) { super.setEndDate(endDate) }

    @DynamoDBAttribute(attributeName="description")
    String getDescription() { return super.getDescription() }
    void setDescription(String description) { super.setDescription(description) }

    @DynamoDBAttribute(attributeName="marketAreaName")
    String getMarketAreaName() { return super.marketAreaName }
    void setMarketAreaName(String marketAreaName) { super.setMarketAreaName(marketAreaName) }

    @DynamoDBAttribute(attributeName="stateAbbrev")
    String getStateAbbrev() { return super.stateAbbrev }
    void setStateAbbrev(String stateAbbrev) { super.setStateAbbrev(stateAbbrev) }

    @DynamoDBAttribute(attributeName="alias")
    String getAlias() { return super.alias }
    void setAlias(String alias) { super.setAlias(alias)  }

    @DynamoDBAttribute(attributeName="priority")
    Integer getPriority() { return super.getPriority() }
    void setPriority(Integer priority) { super.setPriority(priority) }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName="inheritsDatesFromParent")
    Boolean getInheritsDatesFromParent() { return super.getInheritsDatesFromParent() }
    void setInheritsDatesFromParent(Boolean inheritsDatesFromParent) { super.setInheritsDatesFromParent(inheritsDatesFromParent) }

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    @DynamoDBAttribute(attributeName="publishesWithParent")
    Boolean getPublishesWithParent() { return super.getPublishesWithParent() }
    void setPublishesWithParent(Boolean publishesWithParent) { super.setPublishesWithParent(publishesWithParent) }

    @DynamoDBAttribute(attributeName="flyerUrl")
    String getFlyerUrl() { return super.getFlyerUrl() }
    void setFlyerUrl(String flyerUrl) { super.setFlyerUrl(flyerUrl) }

    @DynamoDBAttribute(attributeName="notes")
    List<Note> getNotes() { return super.getNotes() }
    void setNotes(List<Note> notes) { super.setNotes(notes) }

    @DynamoDBAttribute(attributeName="parentFlyers")
    List<String> getParentFlyers() { return super.getParentFlyers() }
    void setParentFlyers(List<String> parentFlyers) { super.setParentFlyers(parentFlyers) }

    @DynamoDBAttribute(attributeName = "children")
    List<FlyerNode> getChildren() { return super.getChildren() }
    void setChildren(List<FlyerNode> children) { super.setChildren(children) }
}
