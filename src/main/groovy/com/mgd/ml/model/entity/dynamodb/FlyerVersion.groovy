package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.*
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType

/**
 * Flyers are described by a parent-child relationship hierarchy
 * called FlyerVersion. The FlyerVersion entity contains all of
 * the information necessary to publish a Flyer for a particular
 * retailer.
 * <p>
 * The FlyerVersion class is annotated with DynamoDB mapper
 * annotations. The DynamoDBTable annotation is a placeholder
 * for the real DynamoDB table name, which will be injected
 * at runtime based on configuration values.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class FlyerVersion {

    // legacy admin flyer version data

    @DynamoDBIndexRangeKey(globalSecondaryIndexName="startDayOfWeek-chainId-index")
    @DynamoDBHashKey(attributeName="chainId")
    Integer chainId

    @DynamoDBRangeKey(attributeName="marketAreaId")
    Integer marketAreaId

    String marketAreaName
    String stateAbbrev
    String alias

    @DynamoDBIndexHashKey(globalSecondaryIndexName="startDayOfWeek-chainId-index")
    Integer startDayOfWeek

    Integer interval
    Integer duration
    Integer rootMarketAreaId
    List<String> parentIds
    FlyerNode hierarchy

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean inheritsDatesFromParent

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean publishesWithParent

	Boolean root

    // chain info
    String chainName
    String chainImageUrl

    // task metadata
    String teamId
    String assignableId
    String assignableType
    String description
    Integer priority
    String flyerUrl
	List<Note> notes
    Long dateUpdated

    @DynamoDBIgnore
    String getCompoundKey() { return (chainId && marketAreaId) ? "${chainId.toString().padLeft(4, "0")}-${marketAreaId.toString().padLeft(6, "0")}" : null }
    void setCompoundKey(String compoundId) {

        def ids = compoundId.tokenize('-')
        if (ids && ids.size() == 2) {
            chainId = ids[0].toInteger()
            marketAreaId = ids[1].toInteger()
        } else {
            throw new Exception("Unable to create FlyerVersion keys from compoundKey value: '${compoundId}'")
        }
    }

    @DynamoDBIgnore
    String getUuid() {
        return (chainId && marketAreaId) ? "00000000-0000-0000-${chainId.toString().padLeft(4, "0")}-${marketAreaId.toString().padLeft(12, "0")}" : null
    }
    void setUuid(String uuid) {

        def ids = uuid.tokenize('-')
        if (ids && ids.size() == 5) {
            chainId = ids[3].toInteger()
            marketAreaId = ids[4].toInteger()
        } else {
            throw new Exception("Unable to create FlyerVersion keys from uuid value: '${uuid}'")
        }
    }
}
