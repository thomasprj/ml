package com.mgd.ml.model.entity.dynamodb

import com.mgd.ml.model.FlyerInterval
import com.mgd.ml.model.StartWeekDay
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Days

/**
 * The LegacyFlyerSchedule describes the published Flyer's lifecycle
 * in the Legacy SQL Server Admin system. This includes the day of week
 * on which the flyer is published, along with the duration (i.e. the
 * start and end date of the offers contained in the Flyer).
 *
 * This entity has been deprecated in favor of a more robust scheduling
 * metaphor.
 */
class LegacyFlyerSchedule {

    StartWeekDay flyerWeekDay
    FlyerInterval flyerInterval
    Integer flyerDuration
    DateTime flyerStartDate
    DateTime flyerEndDate

    static final timeZoneMGD = TimeZone.getTimeZone("US/Central")
    static final dateTimeZoneMGD = DateTimeZone.forTimeZone(timeZoneMGD)

    LegacyFlyerSchedule(Integer startDayOfWeek, FlyerInterval flyerInterval, Integer flyerDuration) {

        this.flyerWeekDay = StartWeekDay.fromId(startDayOfWeek)
        this.flyerInterval = flyerInterval
        this.flyerDuration = flyerDuration
    }

    LegacyFlyerSchedule(Integer startDayOfWeek, FlyerInterval flyerInterval, Integer flyerDuration, DateTime flyerStartDate) {

        this.flyerWeekDay = StartWeekDay.fromId(startDayOfWeek)
        this.flyerInterval = flyerInterval
        this.flyerDuration = flyerDuration

        setFlyerDates(flyerStartDate)
    }

    LegacyFlyerSchedule(DateTime flyerStartDate, DateTime flyerEndDate) {

        this.flyerWeekDay = StartWeekDay.fromDateTime(flyerStartDate)
        this.flyerInterval = FlyerInterval.CUSTOM
        this.flyerDuration = Days.daysBetween(flyerStartDate.toLocalDate(), flyerEndDate.toLocalDate()).days
        this.flyerStartDate = flyerStartDate
        this.flyerEndDate = flyerEndDate
    }

    void setFlyerDates(DateTime flyerStartDate) {

        // calculate the standard MGD flyer start date
        this.flyerStartDate = convertToMGDStartDate(flyerStartDate)

        // the endDate will be 1 minute before the end of the duration
        this.flyerEndDate = this.flyerStartDate
                        .plusDays(flyerDuration)
                        .minusMinutes(1)
    }

    DateTime getOverrideStartDate(Integer newStartDayOfWeek) {

        DateTime newStartDate = null

        if (this.flyerStartDate) {
            if (newStartDayOfWeek < this.flyerWeekDay.toJavaScriptDayOfWeek()) {
                newStartDate = this.flyerStartDate.plusDays(7 +  newStartDayOfWeek - this.flyerWeekDay.toJavaScriptDayOfWeek())
            } else {
                newStartDate = this.flyerStartDate.plusDays(newStartDayOfWeek - this.flyerWeekDay.toJavaScriptDayOfWeek())
            }
        }

        return newStartDate
   }

    DateTime getOverrideEndDate(Integer newStartDayOfWeek) {

        DateTime newEndDate = null

        if (this.flyerEndDate) {
            if (newStartDayOfWeek < this.flyerWeekDay.toJavaScriptDayOfWeek()) {
                newEndDate = this.flyerEndDate.plusDays(7 + newStartDayOfWeek - this.flyerWeekDay.toJavaScriptDayOfWeek())
            } else {
                newEndDate = this.flyerEndDate.plusDays(newStartDayOfWeek - this.flyerWeekDay.toJavaScriptDayOfWeek())
            }
        }

        return newEndDate
    }

    static DateTime convertToMGDStartDate(DateTime dateTime) {

        // the startDate needs to be set to the start of the day
        // for central time zone and represented in UTC
        DateTime tzCentralFlyerStart = dateTime
                .withZone(dateTimeZoneMGD)
                .withTimeAtStartOfDay()

        // convert from Central time to UTC
        return tzCentralFlyerStart.withZone(DateTimeZone.UTC)
    }

}
