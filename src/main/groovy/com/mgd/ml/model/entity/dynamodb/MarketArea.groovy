package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * MarketAreas are the building blocks for Flyers. A Market Area
 * represents a zone of geographic coverage for the retail offers
 * contained in a Flyer instance from a particular retail chain.
 * Multiple chains may serve the same geographic area, but Market
 * Area are not shared across retail chains, and each instance has
 * a unique ID.
 *
 * The geographic coverage is described by a list of ZIP codes
 * served by the MarketArea. In the current implementation,
 * there is a one-to-one relationship between a Market Area instance
 * and a retailer Flyer.
 * <p>
 * The MarketArea class is annotated with DynamoDB mapper
 * annotations. The DynamoDBTable annotation is a placeholder
 * for the real DynamoDB table name, which will be injected
 * at runtime based on configuration values.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class MarketArea {

    @DynamoDBHashKey(attributeName="id")
    Integer id

    String name
    String stateAbbrev
    String alias
    List<String> zips
}
