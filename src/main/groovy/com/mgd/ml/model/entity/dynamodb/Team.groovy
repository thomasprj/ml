package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable

/**
 * A Team represents a grouping of users within an MGD application.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class Team {

    @DynamoDBHashKey(attributeName="id")
    String id

    Long dateCreated
    String name
    List<String> realm
}
