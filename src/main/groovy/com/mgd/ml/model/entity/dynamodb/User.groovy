package com.mgd.ml.model.entity.dynamodb

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped

/**
 * A User represents an authorized user of an MGD application.
 */
@DynamoDBTable(tableName="TABLE_NAME_PLACEHOLDER")
class User {

    @DynamoDBHashKey(attributeName="id")
    String id

    @DynamoDBTyped(DynamoDBAttributeType.BOOL)
    Boolean active

    Long dateUpdated
    String email
    String firstName
    String lastName
    Long lastSignIn
    String password
    List<String> permissions
    List<String> realm
    List<String> roles
    Integer signInCount
    List<String> teamAccess
    String teamId
    String teamName
    String temporaryToken
}
