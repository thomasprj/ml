package com.mgd.ml.service

import com.amazonaws.AmazonServiceException
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.PutObjectRequest
import com.mgd.ml.model.DealQueueState
import com.mgd.ml.model.entity.dynamodb.ArchiveDeal
import com.mgd.ml.model.entity.dynamodb.DealDateCreatedGSI
import com.mgd.ml.model.entity.dynamodb.DealMarketArea
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

/**
 * DealArchiver provides services and data structures
 * for extracting historical Deal records from DynamoDB.
 */
@Component
class PriceHistoryService {

    @Autowired private AmazonDynamoDBClient dynamoDBClient
    @Autowired private DynamoDBMapper dynamoDBMapper
    @Autowired private AmazonS3 s3Client

    @Value('${dynamodb.prefix:core}-deal-${dynamodb.suffix:dev}') String dealsTableName
    @Value('${output.archive.root}/price_history/') String archiveDirectory
    @Value('${sync.path.s3.price_history}') private String archiveS3Path

    private static final String dealsCreatedIndex = "queueState-dateCreated-index"
    private DynamoDBMapperConfig dealsTable

    private static final TimeZone timeZoneMGD = TimeZone.getTimeZone("US/Central")
    private static final DateTimeZone dateTimeZoneMGD = DateTimeZone.forTimeZone(timeZoneMGD)
    private static final DateTimeFormatter dtfYearMonthDate = DateTimeFormat.forPattern("yyyy-MM-dd")

    private static final String UTF_8 = "UTF-8"

    private static final Logger log = LoggerFactory.getLogger(PriceHistoryService.class)


    /**
     * Initialize the name of the DynamoDB table referenced by the DynamoDB mapper
     */
    @PostConstruct
    void initializeTableName() {
        dealsTable = DynamoDBMapperConfig.builder()
                .withTableNameOverride(new DynamoDBMapperConfig.TableNameOverride(dealsTableName))
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .build()
    }


    /**
     * Get the price history for Deals created in the previous month, persist the result set to file
     * and post the file to the S3 bucket used by the EMR data pipeline
     *
     * @return     a map of statistics
     */
    Map generatePriceHistoryArchive() {

        def currentDate = new DateTime()

        DateTime startDateTime = currentDate.minusMonths(1)
                .withZone(dateTimeZoneMGD)
                .withDayOfMonth(1)
                .withTimeAtStartOfDay()

        DateTime endDateTime = startDateTime.plusMonths(1)

        return generatePriceHistoryArchive(startDateTime.millis, endDateTime.millis)
    }


    /**
     * Get the list of Deals created within a given time window, persist the result set to file
     * and post the file to the S3 bucket used by the EMR data pipeline
     *
     * @param startDate  the start date of the window, expressed in millis
     * @param endDate    the end date of the window, expressed in millis
     * @return           a map of statistics
     */
    Map generatePriceHistoryArchive(Long startDate, Long endDate) {

        // create the archive file
        final File archiveFile = generateArchiveFile(startDate)
        def archiveCount = 0
        def skipCount = 0

        // closure for archiving a list of deals from the GSI
        def archiveDeals = { gsiDeals ->

            gsiDeals.each { gsiDeal ->

                try {
                    ArchiveDeal deal = dynamoDBMapper.load(ArchiveDeal.class, gsiDeal.id, dealsTable)
                    // only archive 'Sale Price' type deals
                    if (deal.saleType == 1) {
                        deal.marketAreas.each { marketArea ->
                            FileUtils.writeStringToFile(archiveFile, formatDealForArchive(deal, marketArea), UTF_8, true)
                        }
                        archiveCount++
                    }
                } catch (Exception e) {
                    log.warn("Unable to fetch Deal Id ${gsiDeal.id} from DynamoDB: ${e.message}")
                    skipCount++
                }
            }
        }

        // archive the published deals
        List<DealDateCreatedGSI> published = getDealsForWindow(DealQueueState.PUBLISHED.id, startDate, endDate)
        archiveDeals(published)

        // archive the republished deals
        List<DealDateCreatedGSI> republished = getDealsForWindow(DealQueueState.REPUBLISHED.id, startDate, endDate)
        archiveDeals(republished)

        // move to S3
        pushFileToS3(archiveS3Path, archiveDirectory, archiveFile.name)

        log.info("Successfully saved Price History Archive to S3 bucket for deals created between ${new DateTime(startDate).toString(dtfYearMonthDate)} and ${new DateTime(endDate).toString(dtfYearMonthDate)}; filename: ${archiveFile.name}; S3 bucket: ${archiveS3Path}; records archived: ${archiveCount}; records skipped: ${skipCount}")

        Map results = new HashMap()

        results.put("archiveCount", archiveCount)
        results.put("skipCount", skipCount)
        results.put("archiveFilename", archiveFile.name)

        return results
    }


    /**
     * Get the list of Deals created within a given time window
     *
     * @param startDate  the start date of the window, expressed in millis
     * @param endDate    the end date of the window, expressed in millis
     * @return           the list of dateCreated-index GSI entries
     */
     List<DealDateCreatedGSI> getDealsForWindow(Integer queueState, Long startDate, Long endDate) {

        // deal query for the specified queue state and date range
        String conditionExpression = "queueState = :v_queueState and dateCreated between :v_startDate and :v_endDate"

        // get the list of deals from DynamoDB
        Map<String, AttributeValue> attributes = new HashMap<String, AttributeValue>()
        attributes.put(":v_queueState", new AttributeValue().withN(queueState.toString()))
        attributes.put(":v_startDate", new AttributeValue().withN(startDate.toString()))
        attributes.put(":v_endDate", new AttributeValue().withN(endDate.toString()))

        DynamoDBQueryExpression<DealDateCreatedGSI> queryExpression = new DynamoDBQueryExpression<DealDateCreatedGSI>()
                .withKeyConditionExpression(conditionExpression)
                .withExpressionAttributeValues(attributes)
                .withIndexName(dealsCreatedIndex)
                .withConsistentRead(false)

        return dynamoDBMapper.query(DealDateCreatedGSI.class, queryExpression, dealsTable)
    }


    /**
     * Formats a Deal record for export to the monthly archive file
     *
     * @param deal    the Deal record to fomrat
     * @return        the formatted archive record
     */
    private String formatDealForArchive(ArchiveDeal deal, DealMarketArea marketArea) {

        StringBuilder formattedDealBuilder = new StringBuilder()

        formattedDealBuilder.append("${deal.upc},")
        formattedDealBuilder.append("${marketArea.id},")
        formattedDealBuilder.append("${deal.saleQuantity},")
        formattedDealBuilder.append("${deal.salePrice}\n")

        return formattedDealBuilder.toString()
    }


    /**
     * Generate the Archive output file.
     */
    private File generateArchiveFile(Long millis) {

        // create the base filename
        def baseDate = new DateTime(millis)
                .toDateTimeISO()
                .toString()
                .substring(0, 13)

        File archiveFile = new File(archiveDirectory, "price-history-${baseDate}.csv")
        FileUtils.writeStringToFile(archiveFile, 'upc,marketArea,saleQuantity,salePrice\n', UTF_8, false)

        return archiveFile
    }


    /**
     * Upload the GZIP file to the AWS S3 bucket for monthly archives.
     */
    private Boolean pushFileToS3(String s3Path, String localPath, String fileName) {

        def success = false
        def file = new File(localPath, fileName)

        try {
            s3Client.putObject(new PutObjectRequest(s3Path, fileName, file))
            if (!file.delete()) {
                log.warn("File ${fileName} was successfully copied to S3, but could not be deleted from ${localPath}")
            }
            success = true
        } catch (AmazonServiceException ase) {
            log.warn("Unable to put ${fileName} object to S3 bucket ${s3Path}")
            log.warn("AmazonServiceException: ${ase.message}; Error Code: ${ase.errorCode}")
        } catch (Exception e) {
            log.warn("Unable to put ${fileName} object to S3 bucket ${s3Path}")
            log.warn("Exception: ${e.message}")
        }

        return success
    }
}
