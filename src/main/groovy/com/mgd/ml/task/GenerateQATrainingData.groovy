package com.mgd.ml.task

import com.mgd.ml.service.PriceHistoryService
import com.mgd.ml.configuration.autoconfigure.condition.ConditionalOnTaskProperty
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * The CreateMonthlyDealsArchive task is a Spring scheduled task responsible
 * for creating the monthly Deals archive file for the prior month and posting
 * it to S3.
 */
@Component
@ConditionalOnTaskProperty(name = "tasks.enable.task.price_history", matchIfMissing = true)
class GenerateQATrainingData {

    @Autowired private PriceHistoryService priceHistoryService

    private static final Logger log = LoggerFactory.getLogger(GenerateQATrainingData.class)

    /**
     * Execute the task as per the configured Spring cron schedule (e.g. monthly on the first Monday of the month).
     */
    @Scheduled(cron='${tasks.cron.price_history}')
    void run() {
        log.info("Executing scheduled GenerateQATrainingData task...")

        // only execute if we're within the first week of the month
        def date = new DateTime()
        if (date.dayOfMonth <= 7) {
            def results = priceHistoryService.generatePriceHistoryArchive()
            log.info("A price history archive file named ${results.archiveFilename} was successfully created and posted to S3: ${results.archiveCount} deals were archived; ${results.skipCount} deals were skipped")
        } else {
            log.warn("Schedule was fired after the first week of the month - task will be skipped")
        }
    }
}
